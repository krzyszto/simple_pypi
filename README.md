# Simple PyPi package

## Local development
Install requirements
```
pip install -r requirements.txt
```


## Build package locally

```
pip install .
```

## Usage of the package functions
Write script and import function
```python
from simple_pypi import another_testing_function
```

## Usage of the command line function
Run command in bash
```bash
spypi
```

## Upload package to PyPi

### Build
```
python setup.py bdist
```

### Check
```
twine check dist/*
```

### Upload
```
twine upload -u <USER> -p <PASSWORD> dist /*
```

Package has been created: `https://pypi.org/project/simple-pypi-pycode-pl/0.0.1/`


## Instal from PyPi
```
pip install simple-pypi-pycode-pl==0.0.1
```

### Check which spypi version we are using
```
which spypi
```
