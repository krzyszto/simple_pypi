import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="simple_pypi_pycode_pl",
    version="0.0.1",
    author="Krzysztof Szyda",
    author_email="krzysztof.szyda@netguru.com",
    description="simple pypi package for pycode PL 2020",
    long_description_content_type="text/markdown",
    long_description=long_description,
    packages=setuptools.find_packages(exclude=("tests",)),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
    entry_points={
        'console_scripts': ['spypi=simple_pypi.command_line:main'],
    },
    include_package_data=True,
    url='https://pypi.org/project/simple-pypi-pycode-pl',
)
