import argparse

short_description = 'Simple pypi for pycode PL 2020'
parser = argparse.ArgumentParser(description=short_description)


def f(*args, **kwargs):
    """
    Purpose of this function is for tests to be executed
    """
    return 1


def main():
    args = parser.parse_args()
    f(args)  # So that flake can ignore args being not used.
    print('It works!')
